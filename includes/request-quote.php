<div class="cta-box bg-blue-1 clearfix">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
              <a href="#" class="btn btn-raised btn-primary ripple-effect btn-lg" data-original-title="" title="">
                Book Now
              </a>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <h3>
                Welcome to Optron. Digital Marketing Course
              </h3>
              <p>
                Carefully designed to bring you the best performance, usage and customization experience!
              </p>
            </div>
          </div>
        </div>
      </div>