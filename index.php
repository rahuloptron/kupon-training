<!DOCTYPE html>
<html lang="en" class="gr__pamukovic_com"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>
    Index Page
  </title>
  <meta name="generator" content="#">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/themify-icons.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">
  <link href="owl.carousel/assets/owl.carousel.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">
  <link href="css/animsition.css" rel="stylesheet">
  <link href="css/plugins.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <link rel="shortcut icon" href="#">
  <link rel="apple-touch-icon" href="#">
  <link rel="apple-touch-icon" sizes="72x72" href="#">
  <link rel="apple-touch-icon" sizes="114x114" href="#">
  
    </head>
  
  <body>
    <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">

       <?php include 'includes/header.php' ?>
     
      
      <section id="page" class="container">
        
        <div class="mTop-30">
            <div class="row">
              <div class="col-sm-12 clearfix">
                <div class="hr-link">
                  <hr class="mBtm-50" data-symbol="FEATURED DEALS">
                 
                </div>
              </div>
            </div>
          <div class="row">
            
            <div class="col-sm-4">
                <div class="deal-entry orange">
                  
                  <div class="image">
                    <a href="/details.html" target="_blank" title="#">
                        <img src="images/dummy-course.png" alt="#" class="img-responsive">
                    </a>
                  </div>
                  <!-- /.image -->
                  <div class="title">
                    <a href="/details.html" target="_blank" title="ATLETIKA 3 mēnešu abonements">
                      Digital Marketing Course in Mumbai 1
                    </a>
                  </div>
                  <div class="entry-content">
                    <div class="prices clearfix">
                      <div class="procent">
                        -65%
                      </div>
                      <div class="price">
                        <i class="fa fa-inr">
                        </i>

                        <b>
                          60,00
                        </b>
                      </div>
                      <div class="old-price">
                        <span>
                          <i class="fa fa-inr">
                          </i>
                          171,00
                        </span>
                      </div>
                    </div>
                    <p>
                      Immerse Yourself in the Magic of Cambodia with a Luxurious Eight Day/Seven Night Escape at Two of the World’s Finest Hotels!
                    </p>
                  </div>
                  <!--/.entry content -->
                  <footer class="info_bar clearfix">
                    <ul class="unstyled list-inline row">
                      
                      <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                        <a href="/details.html" class="btn btn-block btn-default btn-raised btn-sm">
                          More
                        </a>
                      </li>
                    </ul>
                  </footer>
                </div>
              </div>
              <!-- /.deal -->
                          <div class="col-sm-4">
                <div class="deal-entry orange">
                  
                  <div class="image">
                    <a href="#" target="_blank" title="#">
                        <img src="images/seo-training-course.jpg" alt="#" class="img-responsive">
                    </a>
                  </div>
                  <!-- /.image -->
                  <div class="title">
                    <a href="#" target="_blank" title="ATLETIKA 3 mēnešu abonements">
                      3Digital Marketing Course in Mumbai 2
                    </a>
                  </div>
                  <div class="entry-content">
                    <div class="prices clearfix">
                      <div class="procent">
                        -65%
                      </div>
                      <div class="price">
                        <i class="fa fa-inr">
                        </i>
                        
                        <b>
                          60,00
                        </b>
                      </div>
                      <div class="old-price">
                        <span>
                          <i class="fa fa-inr">
                          </i>
                          171,00
                        </span>
                      </div>
                    </div>
                    <p>
                      Immerse Yourself in the Magic of Cambodia with a Luxurious Eight Day/Seven Night Escape at Two of the World’s Finest Hotels!
                    </p>
                  </div>
                  <!--/.entry content -->
                  <footer class="info_bar clearfix">
                    <ul class="unstyled list-inline row">
                      
                      <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                        <a href="/p1.html" class="btn btn-block btn-default btn-raised btn-sm">
                          More
                        </a>
                      </li>
                    </ul>
                  </footer>
                </div>
              </div>
              <!-- /.deal -->
                                        <div class="col-sm-4">
                <div class="deal-entry orange">
                  
                  <div class="image">
                    <a href="#" target="_blank" title="#">
                        <img src="images/seo-search-engine-optimization.jpg" alt="#" class="img-responsive">
                    </a>
                  </div>
                  <!-- /.image -->
                  <div class="title">
                    <a href="#" target="_blank" title="ATLETIKA 3 mēnešu abonements">
                      Digital Marketing Course in Mumbai 3
                    </a>
                  </div>
                  <div class="entry-content">
                    <div class="prices clearfix">
                      <div class="procent">
                        -65%
                      </div>
                      <div class="price">
                        <i class="fa fa-inr">
                        </i>
                        
                        <b>
                          60,00
                        </b>
                      </div>
                      <div class="old-price">
                        <span>
                          <i class="fa fa-inr">
                          </i>
                          171,00
                        </span>
                      </div>
                    </div>
                    <p>
                      Immerse Yourself in the Magic of Cambodia with a Luxurious Eight Day/Seven Night Escape at Two of the World’s Finest Hotels!
                    </p>
                  </div>
                  <!--/.entry content -->
                  <footer class="info_bar clearfix">
                    <ul class="unstyled list-inline row">
                      
                      <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                        <a href="#" class="btn btn-block btn-default btn-raised btn-sm">
                          More
                        </a>
                      </li>
                    </ul>
                  </footer>
                </div>
              </div>
              <!-- /.deal -->
                                        <div class="col-sm-4">
                <div class="deal-entry orange">
                  
                  <div class="image">
                    <a href="#" target="_blank" title="#">
                        <img src="images/seo-search-engine-optimization.jpg" alt="#" class="img-responsive">
                    </a>
                  </div>
                  <!-- /.image -->
                  <div class="title">
                    <a href="#" target="_blank" title="ATLETIKA 3 mēnešu abonements">
                      Digital Marketing Course in Mumbai 4
                    </a>
                  </div>
                  <div class="entry-content">
                    <div class="prices clearfix">
                      <div class="procent">
                        -65%
                      </div>
                      <div class="price">
                        <i class="fa fa-inr">
                        </i>
                        
                        <b>
                          60,00
                        </b>
                      </div>
                      <div class="old-price">
                        <span>
                          <i class="fa fa-inr">
                          </i>
                          171,00
                        </span>
                      </div>
                    </div>
                    <p>
                      Immerse Yourself in the Magic of Cambodia with a Luxurious Eight Day/Seven Night Escape at Two of the World’s Finest Hotels!
                    </p>
                  </div>
                  <!--/.entry content -->
                  <footer class="info_bar clearfix">
                    <ul class="unstyled list-inline row">
                      
                      <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                        <a href="#" class="btn btn-block btn-default btn-raised btn-sm">
                          More
                        </a>
                      </li>
                    </ul>
                  </footer>
                </div>
              </div>
              <!-- /.deal -->
                          <div class="col-sm-4">
                <div class="deal-entry orange">
                  
                  <div class="image">
                    <a href="#" target="_blank" title="#">
                        <img src="images/dummy-course.png" alt="#" class="img-responsive">
                    </a>
                  </div>
                  <!-- /.image -->
                  <div class="title">
                    <a href="#" target="_blank" title="ATLETIKA 3 mēnešu abonements">
                      Digital Marketing Course in Mumbai 5
                    </a>
                  </div>
                  <div class="entry-content">
                    <div class="prices clearfix">
                      <div class="procent">
                        -65%
                      </div>
                      <div class="price">
                        <i class="fa fa-inr">
                        </i>
                        
                        <b>
                          60,00
                        </b>
                      </div>
                      <div class="old-price">
                        <span>
                          <i class="fa fa-inr">
                          </i>
                          171,00
                        </span>
                      </div>
                    </div>
                    <p>
                      Immerse Yourself in the Magic of Cambodia with a Luxurious Eight Day/Seven Night Escape at Two of the World’s Finest Hotels!
                    </p>
                  </div>
                  <!--/.entry content -->
                  <footer class="info_bar clearfix">
                    <ul class="unstyled list-inline row">
                      
                      <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                        <a href="#" class="btn btn-block btn-default btn-raised btn-sm">
                          More
                        </a>
                      </li>
                    </ul>
                  </footer>
                </div>
              </div>
              <!-- /.deal -->
              
              <div class="col-sm-4">
                <div class="deal-entry orange">
                  
                  <div class="image">
                    <a href="#" target="_blank" title="#">
                        <img src="images/seo-training-course.jpg" alt="#" class="img-responsive">
                    </a>
                  </div>
                  <!-- /.image -->
                  <div class="title">
                    <a href="#" target="_blank" title="ATLETIKA 3 mēnešu abonements">
                      Digital Marketing Course in Mumbai 6
                    </a>
                  </div>
                  <div class="entry-content">
                    <div class="prices clearfix">
                      <div class="procent">
                        -65%
                      </div>
                      <div class="price">
                        <i class="fa fa-inr">
                        </i>
                        
                        <b>
                          60,00
                        </b>
                      </div>
                      <div class="old-price">
                        <span>
                          <i class="fa fa-inr">
                          </i>
                          171,00
                        </span>
                      </div>
                    </div>
                    <p>
                      Immerse Yourself in the Magic of Cambodia with a Luxurious Eight Day/Seven Night Escape at Two of the World’s Finest Hotels!
                    </p>
                  </div>
                  <!--/.entry content -->
                  <footer class="info_bar clearfix">
                    <ul class="unstyled list-inline row">
                      
                      <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                        <a href="#" class="btn btn-block btn-default btn-raised btn-sm">
                          More
                        </a>
                      </li>
                    </ul>
                  </footer>
                </div>
              </div>
            
          </div>
          <!--/.row -->
        </div>
        <!--/.frame -->
        
        
        
        <!--/.frame -->       
       
      </section>
      <!-- /#page ends -->
       <?php include 'includes/request-quote.php' ?>
      <!-- /.CTA -->
       <?php include 'includes/footer.php' ?>
  </div>
  <!-- /animitsion -->
  <!-- JS files -->
  <script src="js/jquery.min.js">
  </script>
  <script src="js/kupon.js">
  </script>
  <script src="js/bootstrap.min.js">
  </script>
  <script src="js/jquery.animsition.min.js">
  </script>
  <script src="owl.carousel/owl.carousel.js">
  </script>
  <script src="js/jquery.flexslider-min.js">
  </script>
  <script src="js/plugins.js">
  </script>
  
  
    </body>
</html>
