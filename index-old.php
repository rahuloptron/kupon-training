<!DOCTYPE html>
<html lang="en" class="gr__pamukovic_com">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>
       LinkedIn Lead Generation workshop
    </title>
    <meta name="generator" content="#">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/themify-icons.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="owl.carousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/animsition.css" rel="stylesheet">
    <link href="css/plugins.min.css" rel="stylesheet">
    <link href="css/style.css?ver=2" rel="stylesheet">
    <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <link rel="shortcut icon" href="#">
    <link rel="apple-touch-icon" href="#">
    <link rel="apple-touch-icon" sizes="72x72" href="#">
    <link rel="apple-touch-icon" sizes="114x114" href="#">
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23209439-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-23209439-1');
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '280014985511986');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=280014985511986&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
    <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">

    <header>
        
          <div id="nav-wrap">
            <div class="nav-wrap-holder">
              <div class="container" id="nav_wrapper">
                <nav class="navbar navbar-static-top nav-white" id="main_navbar" role="navigation">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
                      <span class="sr-only">
                        Toggle navigation
                                  </span>
                                  <span class="icon-bar">
                                  </span>
                                  <span class="icon-bar">
                                  </span>
                                  <span class="icon-bar">
                                  </span>
                              </button>
                               
                            <a href="index.html" class="navbar-brand logo">
                                <img src="http://www.optron.in/images/logo.png" alt="" class="img-responsive">
                            </a>
                      
                          </div>
                          <!-- Collect the nav links, forms, and other content for toggling -->
                          <div class="collapse navbar-collapse" id="Navbar">
                            <!-- regular link -->
                            <ul class="nav navbar-nav navbar-right">
                              <li class="dropdown">
                                <a href="http://www.optron.in" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  <i class="ti-home">
                                  </i>
                                  Home
                                  <span class="caret">
                                  </span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                  <li>
                                    <a href="/about.html">
                                      About Optron
                                    </a>
                                  </li>
                                  <li>
                                    <a href="/demo.html">
                                      Register for Demo Class
                                    </a>
                                  </li>
                                </ul>
                              </li>
                              
                              <li>
                                <a href="http://www.optron.in/dm1.html">
                                  Full Course
                                </a>
                              </li>
                              
                              <li>
                                <a href="http://www.optron.in/contact.html">
                                  Contact
                                </a>
                              </li>
                            </ul>
                          </div>
                      </nav>
                  </div>
              </div>
              <!-- /.div nav wrap holder -->
          </div>
          <!-- /#nav wrap -->
      </header>


        <section id="inner-page" class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="post-media clearfix">
                        <div id="slider" class="flexslider">
                            <ul class="slides">
                                <li>
                                    <img class="img-responsive" alt="" src="images/lenovo-k8-note-black.png">
                                </li>
                               
                            </ul>
                        </div>
                        <!--/slider -->
                       
                        <!--/.carousel sinc -->
                    </div>
                    <!--/.post media -->
                    <div class="smallFrame shadow bg-white">
                        <i class="ti-calendar">
                  </i>
                        <div class="content">
                           LinkedIn Training for Growing Business
                            <b>
                      16 December 2017 Saturday 
                    </b>
                        </div>
                    </div>

                  
                
                
                          <div class="widget-inner bg-white shadow mBtm-20">
             
                    <!-- Nav tabs -->
                  
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="tab-content">
                          <div class="tab-pane fade active in" id="tab-1">
                            <h2>
                              How to grow business using LinkedIn marketing?
                                                  </h2>
                                                  <p class="lead">
                                                    <i>
                                                      You want to increase sales, attract new customers. You know that calling will not work and if you use pamphlets distribution then you will not get any results. You hire expensive sales professionals and still you are unable to get big orders.
                                                    </i>
                                                  </p>
                                                  
                                                  <h3>What is LinkedIn marketing?</h3>
                                                  <p>
                                                   LinkedIn marketing is not about creating profile page and getting likes on your post. LinkedIn marketing is not just posting your product images and selling online. LinkedIn marketing is more powerful than you think.</p>
                                                   
                                                   <h4>Explore the power of Linkedin Marketing</h4>
                                                   
                                                    <h3>Key Highlights</h3>
                                                    
                                                    
                                                  </p><div class="content">
                                <ul class="tick">
                                    <li>Introduction to LinkedIn and how to start with LinkedIn
                                    </li>
                                    <li>
                                       How to create a professional and attractive profile
                          </strong>
                                    </li>
                                    <li>
                                        How to craft a good LinkedIn post
                                    </li>
                                    <li>
                                        <strong>
                           How to use Canva to create creatives
                          </strong>
                                    </li>
                                    <li>
                                        How to use LinkedIn articles to increase your reach
                                       more people
                                    </li>
                                    <li>
                                       How to get more likes on LinkedIn articles

                                    </li>
                                    <li>
                                        How to write recommendation and how to ask for recommendation
                                    </li>
                                    <li>
                                       How to use LinkedIn search and find potential customers</li>
                                       <li>
                                       Difference between free and paid account</li>
                                        <li>
                                       Joining groups and building community</li>
                                       <li>Many new things which will help you grow your business</li>
                                </ul>
                            </div>
                                                  <div class="section-title-wr">
                                                    <h4 class="section-title left">
                                                      About Trainer
                                                    </h4>
                                                    <p>
                                                   Bhavesh </p>
                                                  </div>
                                                  
                                                  <div class="section-title-wr">
                                                    <h4 class="section-title left">
                                                      Training Details
                                                    </h4>
                                                  </div>
                                                  <table class="table table-bordered table-striped table-hover table-responsive">
                                                    <tbody>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            <strong>
                                                              Event Date:
                                                            </strong>
                                                            16 Dec 2017
                                                          </strong>
                                                        </td>
                                                        <td>
                                                          <strong>
                                                           Day:
                                                          </strong>
                                                          Saturday
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Price:
                                                          </strong>
                                                          Rs.5000.00 
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Breakfast:
                                                          </strong>
                                                          Included
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Lunch:
                                                          </strong>
                                                          Included
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Timing:
                                                          </strong>
                                                          9:30 am to 4:30 pm
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Total Seats:
                                                          </strong>
                                                          10
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Training Type:
                                                          </strong>
                                                          Hands-on
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Live Session:
                                                          </strong>
                                                          Yes
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        
                                                        
                                                        
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                              </div>
                                              <!-- /tab content -->
                                              
                                              
                                              <!-- /tab -->
                                          </div>
                                      </div>
                                      <!--/tabs -->
                                  </div>
                                  <!-- /inner widget -->
                              
                          </div>
                    <div class="widget-inner shadow bg-white mTop-20">
                        <div role="tabpanel" class="tab-pane" id="reviews">
                                                <section class="tab-content">
                                                  <!-- comment start -->
                                                  <div class="comment clearfix">
                                                    
                                                    <header>
                                                      <h3>
                                                        Do I have to bring laptop?
                                                      </h3>
                                                      
                                                      <!--/.stars -->
                                                    </header>
                                                    <div class="comment-content">
                                                      <div class="comment-body clearfix">
                                                        <p>
                                                          Yes, We will be having live training. Your laptop is must.
                                                        </p>
                                                        
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <!-- comment end -->
                                                  <!-- comment start -->
                                                  <div class="comment clearfix">
                                                    
                                                    <header>
                                                      <h3>
                                                       I am not business owner, can I attend this workshop?
                                                      </h3>
                                                      
                                                      <!--/.stars -->
                                                    </header>
                                                    <div class="comment-content">
                                                      <div class="comment-body clearfix">
                                                        <p>
                                                         This workshop is for business owners and marketing professionals. If you are into sales and marketing, you can attend this workshop.
                                                        </p>
                                                        
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <!-- comment end -->
                                                  
                                                  
                                                </section>
                                              </div>
                    </div>

                </div>
                <!-- /col 8 -->
                <div class="col-sm-4">
                    
                    <!--/inner widget -->
                    
                       
                       
                   
                    <div class="terms-and-conditions bg-white shadow mTop-20">
                        <div class="widget-inner">
                            <h3>Key Benefits</h3>
                            <div class="content">
                                <ul class="tick">
                                    <li>
                                      This is
                                        <strong>
                            100% 
                          </strong> practical training
                                    </li>
                                    <li>
                                        Bring your laptop for live sessions
                          </strong>
                                    </li>
                                    <li>
                                       Connect with thousands of LinkedIn members
                                    </li>
                                    <li>
                                        <strong>
                            Breakfast included
                          </strong>
                                    </li>
                                    <li>
                                        Additional fees:
                                        <strong>
                            $15 resort fee per night 
                          </strong> paid at check-in. $100 refundable security deposit required on major credit card at check-in.
                                    </li>
                                    <li>
                                        <strong>
                            Private balconies
                          </strong> are available in the tastefully decorated studios and one-bedroom villas.
                                    </li>
                                    <li>
                                        <strong>
                            Stay cool
                          </strong> as you swim beneath a cascading waterfall in the outdoor pool, where you can watch dive-in movies on Wednesday, Thursday, and Saturday nights.
                                    </li>
                                    <li>
                                        Deal valid:
                                        <b>
                            15.10.2015.
                          </b>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /widget inner -->
                    </div>
                    
                    

                </div>
                <!-- /col 4 - sidebar -->

            </div>
            <!-- /main row -->
        </section>
        <!-- /#page ends -->
        <div class="cta-box clearfix">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                        <a href="http://www.optron.in/inquiry.html" class="btn btn-raised btn-danger ripple-effect btn-lg" data-original-title="" title="">
                            <i class="ti-shopping-cart">
                            </i>
                            &nbsp; Register Now
                        </a>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <h3>
                            Don't miss this great opportunity to grow your business. 
                        </h3>
                        <p>
                            Register now and become a member to stay updated with upcoming workshops and training.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.CTA -->
        <footer id="footer">
            
            <div class="btmFooter">
                <div class="container">
                    <div class="col-sm-7">
                        <p>
                            <strong>
                                Copyright 2017 - 2018
                            </strong>
                            optron
                           
                            <strong>
                                Powered by Optron Digital | <a href="/terms.html">Terms of Use</a> | Privacy Policy
                            </strong>
                        </p>
                    </div>
                  
                </div>
            </div>
        </footer>
    </div>
    <!-- /animitsion -->
    <!-- JS files -->
    <script src="js/jquery.min.js">
    </script>
    <script src="js/kupon.js">
    </script>
    <script src="js/bootstrap.min.js">
    </script>
    <script src="js/jquery.animsition.min.js">
    </script>
    <script src="owl.carousel/owl.carousel.js">
    </script>
    <script src="js/jquery.flexslider-min.js">
    </script>
    <script src="js/plugins.js">
    </script>


    </body>
</html>
