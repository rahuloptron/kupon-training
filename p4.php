<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>
    Training full page
  </title>
  <meta name="generator" content="#" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/themify-icons.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">
  <link href="owl.carousel/assets/owl.carousel.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">
  <link href="css/animsition.css" rel="stylesheet">
  <link href="css/plugins.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <link rel="shortcut icon" href="#">
  <link rel="apple-touch-icon" href="#">
  <link rel="apple-touch-icon" sizes="72x72" href="#">
  <link rel="apple-touch-icon" sizes="114x114" href="#">
  </head>
  
  <body>
    <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
     <header>
        <div class="top-bar bg-light hdden-xs">
          <div class="container">
            <div class="row">
              
                      <div class="pull-right col-sm-6">
                        <ul class="list-inline list-unstyled pull-right">
                         
                          <li>
                           <i class="fa fa-envelope" aria-hidden="true"></i>

                             +919833189090
                        
                          </li>
                          <li>
                         <i class="fa fa-phone" aria-hidden="true"></i>

                            hello@optron.in
                          
                          </li>
                         
                        </ul>
                      </div>
                  </div>
              </div>
          </div>
          <div id="nav-wrap">
            <div class="nav-wrap-holder">
              <div class="container" id="nav_wrapper">
                <nav class="navbar navbar-static-top nav-white" id="main_navbar" role="navigation">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
                      <span class="sr-only">
                        Toggle navigation
                                  </span>
                                  <span class="icon-bar">
                                  </span>
                                  <span class="icon-bar">
                                  </span>
                                  <span class="icon-bar">
                                  </span>
                              </button>
                              <a href="index.html" class="navbar-brand logo">
                                <img src="/images/logo.png" alt="" class="img-responsive">
                            </a>
                          </div>
                          <!-- Collect the nav links, forms, and other content for toggling -->
                          <div class="collapse navbar-collapse" id="Navbar">
                            <!-- regular link -->
                            <ul class="nav navbar-nav navbar-right">
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  <i class="ti-home">
                                  </i>
                                  Home
                                  <span class="caret">
                                  </span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                  <li>
                                    <a href="index.html">
                                      Primary
                                    </a>
                                  </li>
                                  <li>
                                    <a href="index_2.html">
                                      Secondary
                                    </a>
                                  </li>
                                </ul>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  Pages 
                                  <span class="caret">
                                  </span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                  <li>
                                    <a href="details_2.html">
                                      Deal Page 1
                                    </a>
                                  </li>
                                  <li>
                                    <a href="details.html">
                                      Deal Page 2
                                    </a>
                                  </li>
                                  <li>
                                    <a href="results.html">
                                      Search Results
                                    </a>
                                  </li>
                                  <li>
                                    <a href="contact.html">
                                      Contact
                                    </a>
                                  </li>
                                  <li>
                                    <a href="faq.html">
                                      FAQ page
                                    </a>
                                  </li>
                                  <li>
                                    <a href="sumbit.html">
                                      Sumbit deal
                                    </a>
                                  </li>
                                  <li>
                                    <a href="registration.html">
                                      Registration
                                    </a>
                                  </li>
                                  <li>
                                    <a href="cart.html">
                                      Cart Page
                                    </a>
                                  </li>
                                  <li>
                                    <a href="checkout.html">
                                      Checkout
                                    </a>
                                  </li>
                                  <li>
                                    <a href="features.html">
                                      Shortcodes
                                    </a>
                                  </li>
                                </ul>
                              </li>
                              <li>
                                <a href="results.html">
                                  Categories
                                </a>
                              </li>
                              <li>
                                <a href="sumbit.html">
                                  Sumbit
                                </a>
                              </li>
                              <li>
                                <a href="contact.html">
                                  Contact
                                </a>
                              </li>
                            </ul>
                          </div>
                      </nav>
                  </div>
              </div>
              <!-- /.div nav wrap holder -->
          </div>
          <!-- /#nav wrap -->
      </header>
   
      <section id="inner-page" class="container">
        <div class="row">
          <div class="col-lg-8 col-sm-7">
            
            <div class="row mTop-20">
              
              <!-- /col 5 -->
              <div class="col-sm-9 col-lg-12">
                
                <div class="widget-inner bg-white shadow mBtm-20">
                  <div role="tabpanel" id="tabs" class="tabbable responsive">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active">
                        <a href="#tab-1" aria-controls="home" role="tab" data-toggle="tab">
                          Overview
                        </a>
                      </li>
                     
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="tab-content">
                          <div class="tab-pane fade active in" id="tab-1">
                            <h4>
                              Description
                                                  </h4>
                                                  <p class="lead">
                                                    <i>
                                                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                                                    </i>
                                                  </p>
                                                  <p>
                                                    Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
                                                  </p>
                                                  <div class="section-title-wr">
                                                    <h4 class="section-title left">
                                                      Additional details
                                                    </h4>
                                                  </div>
                                                  <table class="table table-bordered table-striped table-hover table-responsive">
                                                    <tbody>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Property Size:
                                                          </strong>
                                                          2300 Sq Ft
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Lot size:
                                                          </strong>
                                                          5000 Sq Ft
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Price:
                                                          </strong>
                                                           <i class="fa fa-inr">
                        </i>23000
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Rooms:
                                                          </strong>
                                                          5
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Bedrooms:
                                                          </strong>
                                                          4
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Bathrooms:
                                                          </strong>
                                                          2
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Garages:
                                                          </strong>
                                                          2
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Roofing:
                                                          </strong>
                                                          New
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Structure Type:
                                                          </strong>
                                                          Bricks
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Year built:
                                                          </strong>
                                                          1991
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Available from:
                                                          </strong>
                                                          1 August 2014
                                                        </td>
                                                        <td>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                  <div class="section-title-wr">
                                                    <h4 class="section-title left">
                                                      Location details
                                                    </h4>
                                                  </div>
                                                  <table class="table table-bordered table-striped table-hover table-responsive">
                                                    <tbody>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            <strong>
                                                              Property Size:
                                                            </strong>
                                                            2300 Sq Ft
                                                          </strong>
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Lot size:
                                                          </strong>
                                                          5000 Sq Ft
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Price:
                                                          </strong>
                                                           <i class="fa fa-inr">
                        </i>23000
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Rooms:
                                                          </strong>
                                                          5
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Bedrooms:
                                                          </strong>
                                                          4
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Bathrooms:
                                                          </strong>
                                                          2
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Garages:
                                                          </strong>
                                                          2
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Roofing:
                                                          </strong>
                                                          New
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Structure Type:
                                                          </strong>
                                                          Bricks
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <strong>
                                                            Year built:
                                                          </strong>
                                                          1991
                                                        </td>
                                                        <td>
                                                          <strong>
                                                            Available from:
                                                          </strong>
                                                          1 August 2014
                                                        </td>
                                                        <td>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                              </div>
                                              <!-- /tab content -->
                                              
                                              
                                              <!-- /tab -->
                                          </div>
                                      </div>
                                      <!--/tabs -->
                                  </div>
                                  <!-- /inner widget -->
                              </div>

                  <div role="tabpanel" id="tabs" class="tabbable responsive">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                     
                      <li role="presentation" class="active">
                        <a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab" class="btn-primary">
                          reviews
                        </a>
                      </li>
                      
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="tab-content">
                          
                                              <!-- /tab content -->
                                              <div role="tabpanel" class="tab-pane active" id="reviews">
                                                <section class="tab-content">
                                                  <!-- comment start -->
                                                  <div class="comment clearfix">
                                                    <div class="comment-avatar">
                                                        <img src="http://placehold.it/80x80" alt="avatar">
                                                    </div>
                                                    <header>
                                                      <h3>
                                                        Really Nice!
                                                      </h3>
                                                      <div class="comment-meta stars">
                                                        <i class="ti-star">
                                                        </i>
                                                        <i class="ti-star">
                                                        </i>
                                                        <i class="ti-star">
                                                        </i>
                                                        <i class="ti-start">
                                                        </i>
                                                        <i class="ti-star">
                                                        </i>
                                                        | Today, 10:31
                                                      </div>
                                                      <!--/.stars -->
                                                    </header>
                                                    <div class="comment-content">
                                                      <div class="comment-body clearfix">
                                                        <p>
                                                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        </p>
                                                        <a href="#l" class="btn btn-sm btn-default  btn-raised ripple-effect pull-right">
                                                          <i class="fa fa-reply">
                                                          </i>
                                                          Reply
                                                        </a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <!-- comment end -->
                                                  <!-- comment start -->
                                                  <div class="comment clearfix">
                                                    <div class="comment-avatar">
                                                        <img src="http://placehold.it/80x80" alt="avatar">
                                                    </div>
                                                    <header>
                                                      <h3>
                                                        Worth to Buy!
                                                      </h3>
                                                      <div class="comment-meta stars">
                                                        <i class="ti-star">
                                                        </i>
                                                        <i class="ti-star">
                                                        </i>
                                                        <i class="ti-star">
                                                        </i>
                                                        <i class="ti-start">
                                                        </i>
                                                        <i class="ti-star">
                                                        </i>
                                                        | Today, 10:31
                                                      </div>
                                                      <!--/.stars -->
                                                    </header>
                                                    <div class="comment-content">
                                                      <div class="comment-body clearfix">
                                                        <p>
                                                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        </p>
                                                        <a href="#l" class="btn btn-sm btn-default  btn-raised ripple-effect pull-right">
                                                          <i class="fa fa-reply">
                                                          </i>
                                                          Reply
                                                        </a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <!-- comment end -->
                                                  <h4 class="margin-bottom-20">
                                                    Post a Comment
                                                  </h4>
                                                  <form action="#" method="post" id="comment-form" class="comments" novalidate="novalidate">
                                                    <fieldset>
                                                      <div class="row space-xs">
                                                        <div class="col-md-6">
                                                          <div>
                                                            <input type="text" name="name" id="name" placeholder="Name" class="form-control">
                                                          </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                          <div>
                                                            <input type="text" name="email" id="email" placeholder="Email" class="form-control">
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="space-xs">
                                                        <div>
                                                          <textarea rows="8" name="message" id="message" placeholder="Write comment here ..." class="form-control">                                                          </textarea>
                                                        </div>
                                                      </div>
                                                      <p>
                                                        <button type="submit" class="btn btn-green btn-raised ripple-effect">
                                                          Submit Comment
                                                        </button>
                                                      </p>
                                                    </fieldset>
                                                    
                                                  </form>
                                                </section>
                                              </div>
                                              
                                              <!-- /tab -->
                                          </div>
                                      </div>
                                      <!--/tabs -->
                                  </div>
                                  <!-- /inner widget -->
                              </div>
                          </div>
                          
                          
                      </div>
                      <!-- col 7 -->
                  </div>
              </div>


              <!-- /col 8 -->
              <div class="col-sm-4">
                <div class="widget-inner bg-white shadow">
                  <div class="buyPanel bg-white Aligner text-center">
                    <div class="content">
                      <div class="deal-content">
                        <h3>
                         Seo Training Course
                        </h3>
                        <p>
                          Stay for two, with optional all-inclusive package. Dates into September
                        </p>
                      </div>
                      <div class="price">
                        <h1>
                           <i class="fa fa-inr">
                        </i> 12000
                        </h1>
                      </div>
                      <div class="buy-now mBtm-30">
                        <a class="btn btn-success btn-lg btn-raised ripple-effect btn-block" data-toggle="modal" data-target="#myModal">
                          Book Now
                          <span class="link animate">
                          </span>
                        </a>
                      </div>
                      <div class="dealAttributes">
                        <div class="valueInfo bg-light shadow">
                          <div class="value">
                            <p class="value">
                               <i class="fa fa-inr">
                        </i>2,475
                            </p>
                            <p class="text">
                              Value
                            </p>
                          </div>
                          <div class="discount">
                            <p class="value">
                              59%
                            </p>
                            <p class="text">
                              Discount
                            </p>
                          </div>
                          <div class="save">
                            <p class="value">
                               <i class="fa fa-inr">
                        </i>1,476
                            </p>
                            <p class="text">
                              SAVINGS
                            </p>
                          </div>
                        </div>
                        
                        <!--/.social sharing -->
                      </div>
                    </div>
                  </div>
                </div>
                <!--/inner widget -->
                
                <div class="terms-and-conditions bg-white shadow mTop-20">
                  <div class="widget-inner">
                    <hr data-symbol="ADDITIONAL INFO">
                    <div class="content">
                      <ul class="tick">
                        <li>
                          Stay
                          <strong>
                            for four 
                          </strong>
                          in a studio or one-bedroom villa
                        </li>
                        <li>
                          The offer includes:
                          <strong>
                            3 night at hotel ***
                          </strong>
                        </li>
                        <li>
                          Buy deal as a gift to other person easy
                        </li>
                        <li>
                          <strong>
                            Breakfast included
                          </strong>
                        </li>
                        <li>
                          Additional fees:
                          <strong>
                             <i class="fa fa-inr">
                        </i>15 resort fee per night 
                          </strong>
                          paid at check-in.  <i class="fa fa-inr">
                        </i>100 refundable security deposit required on major credit card at check-in.
                        </li>
                        <li>
                          <strong>
                            Private balconies
                          </strong>
                          are available in the tastefully decorated studios and one-bedroom villas.
                        </li>
                        <li>
                          <strong>
                            Stay cool
                          </strong>
                          as you swim beneath a cascading waterfall in the outdoor pool, where you can watch dive-in movies on Wednesday, Thursday, and Saturday nights.
                        </li>
                        <li>
                          Deal valid:
                          <b>
                            15.10.2015.
                          </b>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- /widget inner -->
                </div>
                <div class="widget">
                  <div class="deal-entry green deal-entry-sm mTop-20">
                    <div class="offer-discount">
                      -71%
                    </div>
                    <div class="image ripple-effect">
                      <a href="#" target="_blank" title="#">
                          <img src="http://placehold.it/700x400" alt="#" class="img-responsive">
                      </a>
                      <span class="bought">
                        <i class="ti-tag">
                        </i>
                        21
                      </span>
                      <div class="caption">
                        <h5 class="media-heading">
                          <a href="#">
                            Plaza Resort Hotel &amp; SPA
                          </a>
                        </h5>
                      </div>
                    </div>
                    <footer class="info_bar clearfix">
                      <div class="prices clearfix">
                        <div class="procent">
                          -71%
                        </div>
                        <div class="price">
                          <i class="ti-money">
                          </i>
                          
                          <b>
                            54,00
                          </b>
                        </div>
                        <div class="old-price">
                          <span>
                            <i class="ti-money">
                            </i>
                            171,00
                          </span>
                        </div>
                      </div>
                    </footer>
                  </div>
                  <!-- /deal entry -->
                  <div class="deal-entry green deal-entry-sm">
                    <div class="offer-discount">
                      -71%
                    </div>
                    <div class="image ripple-effect">
                      <a href="#" target="_blank" title="#">
                          <img src="http://placehold.it/700x400" alt="#" class="img-responsive">
                      </a>
                      <span class="bought">
                        <i class="ti-tag">
                        </i>
                        21
                      </span>
                      <div class="caption">
                        <h5 class="media-heading">
                          <a href="#">
                            Plaza Resort Hotel &amp; SPA
                          </a>
                        </h5>
                      </div>
                    </div>
                    <footer class="info_bar clearfix">
                      <div class="prices clearfix">
                        <div class="procent">
                          -71%
                        </div>
                        <div class="price">
                          <i class="ti-money">
                          </i>
                          
                          <b>
                            54,00
                          </b>
                        </div>
                        <div class="old-price">
                          <span>
                            <i class="ti-money">
                            </i>
                            171,00
                          </span>
                        </div>
                      </div>
                    </footer>
                  </div>
                  <!-- /deal entry -->
                </div>
                <!-- /.widget -->
              </div>
              <!-- /col 4 - sidebar -->
              
          </div>
          <!-- /main row -->
      </section>
      <!-- /#page ends -->
       <?php include 'includes/request-quote.php' ?>
      <!-- /.CTA -->
      <?php include 'includes/footer.php' ?>
  </div>
  <!-- /animitsion -->
  <!-- JS files -->
  <script src="js/jquery.min.js">
  </script>
  <script src="js/kupon.js">
  </script>
  <script src="js/bootstrap.min.js">
  </script>
  <script src="js/jquery.animsition.min.js">
  </script>
  <script src="owl.carousel/owl.carousel.js">
  </script>
  <script src="js/jquery.flexslider-min.js">
  </script>
  <script src="js/plugins.js">
  </script>
  
  </body>
  
</html>